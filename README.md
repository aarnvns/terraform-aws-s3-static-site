# AWS Static Site Terraform module

Terraform module which creates a static site in AWS using S3 and CloudFront.

## Usage

```hcl
module "aws-s3-static-site" {
  source = "git@gitlab.com:aarnvns/terraform-aws-s3-static-site.git"

  do_create_redirect_site           = true
  route53_hosted_zone_name          = "unmonitized.com"
  route53_static_site_name          = "www"

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
```

## Use cases

This S3 static-site modules contains the following features:
- [x] HTTPS with automatic certificate renewal via ACM
- [x] flexible ability delegating the DNS records for the site to Route 53
- [x] ability to optionally create a site for redirecting to the static site (e.g. redirecting from example.com to www.example.com)

### DNS Management

This module supports creating static sites in the following methods:

#### subdomain.example.org

You're creating a static-site that's a subdomain of a parent domain such as `example.org`.

In this configuration, an administrator will delegate `subdomain.example.org` as a hosted-zone for use
by this module.  No redirect site would be used.

Example inputs for this scenario:

| Name | Value | Notes |
|------|-------|-------|
| route53_hosted_zone_name | subdomain.example.org | |
| route53_static_site_name | "" | Leaving this blank makes `subdomain.example.org` the domain name |
| do_create_redirect_site | false | |

#### www.example.org with redirect from apex domain

The ability to provide a redirect site (usually from `example.org` to `www.example.org`) requires
delegating the entire `example.org` apex domain to Route53.

This approach may not be acceptable in scenarios where IT administrators must maintain other DNS entries
(such as MX entries for e-mail), I can't risk this module interfering with the apex domain.

Example inputs for this scenario:

| Name | Value | Notes |
|------|-------|-------|
| route53_hosted_zone_name | example.org | |
| route53_static_site_name | www | |
| route53_redirect_site_name | "" | leaving this blank creates a redirect from `example.org` -> `www.example.org` |
| do_create_redirect_site | true | |

#### www.example.org without redirect from apex domain

This scenario is for situations where only the `www.example.org` DNS name will be delegated (for common situations
where an IT department owns `example.org` and an engineering department owns `www.example.org`).

Because it is desirable to have an A record on the apex domain that redirects to `www`, you will need a DNS provider
that supports the ability on its own.

Example inputs for this scenario:

| Name | Value | Notes |
|------|-------|-------|
| route53_hosted_zone_name | www.example.org | |
| route53_static_site_name | "" | Leaving this blank makes `www.example.org` the domain name |
| do_create_redirect_site | false | |


## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.12.6, < 0.14 |
| aws | ~> 2.57 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.57 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| route53_hosted_zone_name | Name of the Route53 Hosted Zone | String | | true |
| route53_static_site_name | Name of the Route53 DNS record for the static site | String | | true |
| route53_redirect_site_name | Name of the Route53 DNS record for the redirect site | String | "" | false |
| do_create_redirect_site | Whether to create an additional site that redirects to the static site | Boolean | | true | 
| cloudfront_price_class | Which performance/cost class to use for CloudFront | String | PriceClass_100 | false |

## Outputs

| Name | Description |
|------|-------------|
| aws_route53_zone_nameservers | A list of the nameservers for the Route53 hosted zone |

## License

Free with attribution (see LICENSE.md)
