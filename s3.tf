resource "aws_s3_bucket" static_site_bucket {
  bucket = local.static_site_domain_name
  acl    = "public-read"

  versioning {
    enabled = true
  }

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AddPerm",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::${local.static_site_domain_name}/*"
            ]
        }
    ]
}
EOF

  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_s3_bucket" redirect_site_bucket {
  bucket = var.route53_hosted_zone_name
  acl = "public-read"

  versioning {
    enabled = true
  }

  website {
    redirect_all_requests_to = "https://${local.static_site_domain_name}"
  }

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AddPerm",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::${var.route53_hosted_zone_name}/*"
            ]
        }
    ]
}
EOF

  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}
