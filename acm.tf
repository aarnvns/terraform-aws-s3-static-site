resource "aws_acm_certificate" domain_acm {
  domain_name               = local.static_site_domain_name
  validation_method         = "DNS"
  subject_alternative_names = var.do_create_redirect_site ? [var.route53_hosted_zone_name] : []

  tags = var.tags
}

resource "aws_route53_record" domain_acm_dns_validation {
  count = var.do_create_redirect_site ? 2 : 1

  name    = element(aws_acm_certificate.domain_acm.domain_validation_options.*.resource_record_name, count.index)
  type    = element(aws_acm_certificate.domain_acm.domain_validation_options.*.resource_record_type, count.index)
  records = [element(aws_acm_certificate.domain_acm.domain_validation_options.*.resource_record_value, count.index)]
  zone_id = aws_route53_zone.hosted_zone.zone_id
  ttl     = 60
}

resource "aws_acm_certificate_validation" domain_acm_certificate_validation {
  certificate_arn         = aws_acm_certificate.domain_acm.arn
  validation_record_fqdns = aws_route53_record.domain_acm_dns_validation.*.fqdn
}
