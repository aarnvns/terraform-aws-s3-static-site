locals {
  static_site_domain_name = "${var.route53_static_site_name}.${var.route53_hosted_zone_name}"
  redirect_site_domain_name = length(var.route53_static_site_name) == 0 ? var.route53_hosted_zone_name : "${var.route53_redirect_site_name}.${var.route53_hosted_zone_name}"
}
