resource "aws_cloudfront_distribution" static_site_cloudfront {
  origin {
    domain_name = aws_s3_bucket.static_site_bucket.website_endpoint
    origin_id = aws_s3_bucket.static_site_bucket.website_endpoint

    custom_origin_config {
      # http-only is required for bucket redirects with CloudFront
      origin_protocol_policy = "http-only"
      http_port              = 80
      https_port             = 443
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  enabled             = true
  default_root_object = "index.html"

  aliases = [local.static_site_domain_name]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.static_site_bucket.website_endpoint

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  price_class = var.cloudfront_price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate_validation.domain_acm_certificate_validation.certificate_arn
    ssl_support_method  = "sni-only"
  }

  tags = var.tags
}

resource "aws_cloudfront_distribution" redirect_site_distribution {
  origin {
    domain_name = aws_s3_bucket.redirect_site_bucket.website_endpoint
    origin_id = aws_s3_bucket.redirect_site_bucket.website_endpoint

    custom_origin_config {
      # http-only is required for bucket redirects with CloudFront
      origin_protocol_policy = "http-only"
      http_port              = 80
      https_port             = 443
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  enabled         = true
  aliases         = [var.route53_hosted_zone_name]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.redirect_site_bucket.website_endpoint

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
  }

  price_class = var.cloudfront_price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate_validation.domain_acm_certificate_validation.certificate_arn
    ssl_support_method  = "sni-only"
  }

  tags = var.tags
}
