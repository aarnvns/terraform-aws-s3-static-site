variable route53_hosted_zone_name {
  type        = string
  description = "Name of the hosted zone that will be used for the static site"
}

variable route53_static_site_name {
  type = string
  description = "Name of the Route53 record to create for the static site, e.g. \"www\"."
}

variable route53_redirect_site_name {
  type = string
  description = "Name of the Route53 redirect site (if enabled)."
  default = ""
}

variable do_create_redirect_site {
  type        = bool
  description = "If set, the site will be served under www.domain_name (and domain_name will redirect to it)"
}

variable cloudfront_price_class {
  type = string
  description = "See https://docs.aws.amazon.com/cloudfront/latest/APIReference/API_DistributionConfig.html"
  default = "PriceClass_100"
}

variable tags {
  type = map(string)
  description = "Tags to apply to created resources"
  default = {}
}
