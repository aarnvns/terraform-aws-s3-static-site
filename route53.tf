resource "aws_route53_zone" hosted_zone {
  name = var.route53_hosted_zone_name

  lifecycle {
    prevent_destroy = true
  }

  tags = var.tags
}

resource "aws_route53_record" static_site_dns_record {
  zone_id = aws_route53_zone.hosted_zone.zone_id
  name    = "www"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.static_site_cloudfront.domain_name
    zone_id                = aws_cloudfront_distribution.static_site_cloudfront.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" redirect_site_dns_record {
  zone_id = aws_route53_zone.hosted_zone.zone_id
  name    = ""
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.redirect_site_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.redirect_site_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

